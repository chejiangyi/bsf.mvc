springboot的扩展实现，让springboot开发更加简单；形成demo模板，以后开发更方便。


1. 自动requestmapping(无需配置)实现。
2. freemarker java扩展实现，使freemarker更加便于使用。
3. request 参数大小写兼容实现，传入参数不再区分大小写。
    date日期参数兼容，支持对象绑定对public字段的自动映射。
4. url 大小写兼容，不敏感。
5. 提供默认分页控件。
6. 支持.net mvc的写法。

1. 自动requestmapping(无需配置)实现。
<pre class="brush:html;gutter:true;">
 1 /**
 2  * sprinboot扩展实现，自动requestmapping（public，返回值为ModelAndView类型/或子类）的方法
 3  */
 4 public class SpringMvcDemoController extends SpringMvcController {
 5     public ModelAndView index(Integer a,String b)
 6     {
 7         return this.pageVisit(m->{
 8             //分页控件实现说明
 9             new Pager1(1,20).setPageSize(10).out();
10         });
11     }
12 
13 
14     public ModelAndView index2()
15     {
16         return new ModelAndView();
17     }
18 }
</pre>
<pre class="brush:html;gutter:true;">
application.properties 文件中配置，加快自动映射启动速度。
#bsf.mvc自动requestmapping注入的包路径,逗号分割多个
bsf.mvcpackage=com.bsf.mvc.webdemo.controller 
</pre>

2. freemarker java扩展实现，使freemarker更加便于使用。
freemarker页面
<pre class="brush:html;gutter:true;">
${Html.s("pagetitle","编辑分类")}
${Html.g("pagetitle")}
${Html.p(model.createtime)}
${Html.w(c.selected,"selected='selected'","")}
</pre>
 后台代码
<pre class="brush:html;gutter:true;">
/**
 * TemplateProvider 缩写简写扩展，方便页面模板里面使用
可以被继承，不断扩展
 */
public class SimpleTemplateProvider extends TemplateProvider {
    /**
     * getattr方法 缩写
     */
    public Object g(String key) {
        return getattr(key);
    }

    /**
     * setattr方法 缩写
     */
    public void s(String key, Object value) {
        setattr(key, value);
    }

    /**
     * where 简写
     */
    public Object w(boolean istrue, Object trueObj, Object falseObj) {
        return where(istrue, trueObj, falseObj);
    }

    /**
     * print 缩写
     */
    public String p(Object o) {
        return print(o);
    }
}
</pre>
 5. 提供默认分页控件。

freemaker
<pre class="brush:html;gutter:true;">
<#macro _pager formid="searchForm">
    ${pagehtml!}
    <script type="text/javascript">
　　//js分页回调
    function pagerfunction(pageindex) {
        $("#${formid}").prepend("<input type='hidden' name='pageindex' value='"+pageindex+"'/>");
        $("#${formid}").prepend("<input type='hidden' name='pagesize' value='"+'${pagesize!}'+"'/>");
        $("#${formid}").submit();
    }
    </script>
</#macro>
</pre>
 后台代码
<pre class="brush:html;gutter:true;">
new Pager1(pageindex,count).setPageSize(10).out();
</pre>

6. 支持.net mvc的写法。
<pre class="brush:html;gutter:true;">
/**
 * 扩展springboot 模拟.net mvc的写法
 * 需要继承 NetController
 */
public class NetMvcDemoController extends NetMvcController {
    public ActionResult index(Integer a,String b)
    {
        //分页控件实现说明
        new Pager1(1,20).setPageSize(10).out();
        return view();
    }

    public ActionResult index2()
    {
        List a = new ArrayList<String>();
        return view("/netmvcdemo/index2",a);
    }

    public ActionResult json()
    {
        List a = new ArrayList<String>();
        return json(a);
    }
}
</pre>
 

 