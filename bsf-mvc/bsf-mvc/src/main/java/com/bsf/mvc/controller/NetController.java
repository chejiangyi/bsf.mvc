package com.bsf.mvc.controller;

import com.bsf.mvc.HttpContext;
import org.springframework.web.servlet.ModelAndView;

public class NetController extends BaseController {
    public class ViewBag {
        public void set(String key, Object value) {
            HttpContext.getRequest().setAttribute(key, value);
        }

        public Object get(String key) {
            return HttpContext.getRequest().getAttribute(key);
        }
    }

    public class ActionResult extends ModelAndView {
public ActionResult()
{
    super();
}
    }

    protected ActionResult view() {
        return view(null);
    }

    protected ActionResult view(Object model) {
        return view(null, model);
    }

    protected ActionResult view(String viewpath, Object model) {
        ActionResult r = new ActionResult();
        r.setViewName(viewpath);
        r.addObject("model", model);
        return r;
    }

    public class JsonResult extends ActionResult {
        public Object Data;
    }

    protected JsonResult json(Object model) {
        JsonResult j = new JsonResult();
        j.Data = model;
        //j.addObject("model",model);
        return j;
    }
}
