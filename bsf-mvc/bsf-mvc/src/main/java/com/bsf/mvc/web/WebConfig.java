package com.bsf.mvc.web;

import com.bsf.base.BsfConfig;
import com.bsf.mvc.HttpContextFilter;
import com.bsf.mvc.controller.AutoRequestMappingProvider;
import com.bsf.mvc.filter.CaseInsensitiveRequestParameterNameFilter;
import com.bsf.mvc.interceptor.NetHandlerInterceptor;
import com.bsf.util.PropertiesUtil;
import com.bsf.util.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.mvc.method.RequestMappingInfoHandlerMapping;

@Configuration
@EnableAutoConfiguration
public class WebConfig extends WebMvcConfigurerAdapter {

    @Autowired
    RequestMappingInfoHandlerMapping requestMappingInfoHandlerMapping;

    //拦截器注册
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new NetHandlerInterceptor());
        super.addInterceptors(registry);
    }

    //注册上下文 request和response 过滤器
    @Bean
    public FilterRegistrationBean httpContextFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean(new HttpContextFilter());
        registration.addUrlPatterns("/*");
        registration.setName("HttpContextFilter");
        registration.setOrder(1);
        return registration;
    }

    //忽略request参数大小写 过滤器
    @Bean
    public FilterRegistrationBean caseInsensitiveRequestParameterNameFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean(new CaseInsensitiveRequestParameterNameFilter());
        registration.addUrlPatterns("/*");
        registration.setName("CaseInsensitiveRequestParameterNameFilter");
        registration.setOrder(1);
        return registration;
    }

//        @Bean
//        public ServletRegistrationBean testServletRegistration() {
//            ServletRegistrationBean registration = new ServletRegistrationBean(new TestServlet());
//            registration.addUrlMappings("/hello");
//            return registration;
//        }

//        @Bean
//        public ServletListenerRegistrationBean<TestListener> testListenerRegistration(){
//            ServletListenerRegistrationBean<TestListener> registration = new ServletListenerRegistrationBean<TestListener>(new TestListener());
//            return registration;
//        }

    //首页配置
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        AutoRequestMappingProvider p = new AutoRequestMappingProvider(requestMappingInfoHandlerMapping);
        if(!StringUtils.isEmpty(BsfConfig.getMvcPackage())) {
            String[] packagenames= StringUtils.split(StringUtil.nullToEmpty(BsfConfig.getMvcPackage()),',');
            p.init(packagenames);
        }
        else
        {
            p.init();
        }
        super.addViewControllers(registry);
    }

    //url 配置调整
    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {


        AntPathMatcher matcher = new AntPathMatcher();
        matcher.setCaseSensitive(false);//允许url大小写不敏感
        configurer.setPathMatcher(matcher)
                .setUseSuffixPatternMatch(false)//表示设计人员希望系统对外暴露的URL不会识别和匹配.*后缀。在这个例子中，就意味着Spring会将9781-1234-1111.1当做一个{isbn}参数传给BookController。
                .setUseTrailingSlashMatch(true);//表示系统不区分URL的最后一个字符是否是斜杠/。在这个例子中，就意味着http://localhost:8080/books/9781-1234-1111和http://localhost:8080/books/9781-1234-1111/含义相同。
    }
}
