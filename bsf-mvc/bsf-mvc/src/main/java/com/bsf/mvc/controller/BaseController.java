package com.bsf.mvc.controller;

import com.bsf.mvc.web.CustomWebDataBinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BaseController {
    //mvc 到后台方法参数转换的注册绑定
    @InitBinder
    protected void initBinder(WebDataBinder binder){
        new CustomWebDataBinder().initBinder(binder);
    }

    @Autowired
    protected HttpServletRequest request;

    @Autowired
    protected HttpServletResponse response;


}
