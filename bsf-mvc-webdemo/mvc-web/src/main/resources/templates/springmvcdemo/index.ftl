﻿<#import "../_layout.ftl" as layout>
${Html.s("pagetitle","springmvc使用演示")}
<@layout._layout>
<div class="head">
    <div class="title">
       ${pagetitle}
    </div>
</div>
<div>
    <a href="/category/categoryadd/" class="btn1" target="_blank">新增</a>
</div>
<div class="orderlist m10 myorder">
    <form action="/category/categoryindex/" method="post" id="searchForm">
        <div class="search">

            <label>分类名</label><input type="text" class="text midtext" style="width:150px;" name="name" value="${name!}" />

            <label>备注</label><input type="text" class="text midtext" style="width:200px;" name="remark" value="${remark!}" />
            <input type="submit" class="btn1" value="搜索" accesskey="S" />
        </div>
    </form>
    <div class="tab_cont">
        <div class="List">
            <@_list/>
        </div>
    </div>
</div>
    <script>

    </script>
</@layout._layout>
<#macro _list >
<table class="mytable" width="100%">
    <tr>
        <th style="width:5%">Id</th>
        <th style="width:15%">名称</th>
        <th style="width:50%">备注</th>
        <th style="width:15%">创建时间</th>
        <th style="width:15%">操作</th>
    </tr>
    <#--<#list model as item>-->
        <#--<tr data-id="${item.id}">-->
        <#--<td>${item.id}</td>-->
        <#--<td style="text-align:left;">${item.name}</td>-->
        <#--<td style="text-align:left" title="${item.remark}">${html.substring3(item.remark,55)}</td>-->
        <#--<td>${html.p(item.createtime)}</td>-->
        <#--<td>-->
            <#--<a href="#" class="edit">修改</a>-->
            <#--<a href="#" class="del">删除</a>-->
            <#--<a href="#" class="reload" title="通知客户端重新加载配置">客户端重启</a>-->
            <#--<a href="/project/projectindex/?categoryid=${item.id}" target="_blank">相关项目</a>-->
        <#--</td>-->
    <#--</tr>-->
    <#--</#list>-->
</table>
<div class="total pt10">
    <@layout._pager/>
</div>
</#macro>