<#--头部样式填充-->
<#macro _style_head>
</#macro>

<#--<#setting boolean_format="true,false" >-->
<#macro _leftmenu>
<div class="leftmenu fl">
    <ul class="level1">
        <li>
            <p>配置管理<i></i></p>
            <ul class="level2">
                <li><a href="/project/projectindex/">项目管理</a></li>
            </ul>
            <#if user.isAdmin()==true>
                <ul class="level2">
                    <li><a href="/category/categoryindex/">分类管理</a></li>
                </ul>
                <ul class="level2">
                    <li><a href="/config/configindex/">配置管理</a></li>
                </ul>
            </#if>
        </li>
        <#if user.isAdmin()==true>
            <li>
                <p>日志查看<i></i></p>
                <ul class="level2">
                    <li><a href="/error/errorindex/">错误日志</a></li>
                </ul>
                <ul class="level2">
                    <li><a href="/log/logindex/">常用日志</a></li>
                </ul>
            </li>
            <li>
                <p>系统配置<i></i></p>
                <ul class="level2">
                    <li><a href="/systemconfig/systemconfigindex/">系统配置</a></li>
                </ul>
            </li>
            <li>
                <p>用户管理<i></i></p>
                <ul class="level2">
                    <li><a href="/user/index/">用户管理</a></li>
                </ul>
            </li>
        <#--<li>-->
        <#--<p>系统维护<i></i></p>-->
        <#--<ul class="level2">-->
        <#--<li><a href="@Url.Action(" ConfigExport", "Operation")">项目配置导出</a></li>-->
        <#--<li><a href="@Url.Action(" Configimport", "Operation")">项目配置导入</a></li>-->
        <#--</ul>-->
        <#--</li>-->
        </#if>
    </ul>
</div>
</#macro>

<#macro _footer>
by 车江毅
</#macro>
<#--分页控件-->
<#macro _pager formid="searchForm">
    ${pagehtml!}
    <script type="text/javascript">
    function pagerfunction(pageindex) {
        $("#${formid}").prepend("<input type='hidden' name='pageindex' value='"+pageindex+"'/>");
        $("#${formid}").prepend("<input type='hidden' name='pagesize' value='"+'${pagesize!}'+"'/>");
        $("#${formid}").submit();
    }
    </script>
</#macro>

${Html.s("_html",html)}
<#macro _layout stylehead=_style_head foot=_footer leftmenu=_leftmenu >
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width"/>
    <title> ${Html.g("pagetitle")!} - webdemo</title>
    <link href="/content/css/css.css" rel="stylesheet"/>
    <link href="/content/HighChartPackCss.css" rel="stylesheet"/>
    <link href="/content/pager.css" rel="stylesheet"/>
    <script src="/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
    <script src="/content/datepicker/wdatepicker.js"></script>
    <script src="/content/js/common.js"></script>
    <script src="/scripts/jquery.unobtrusive-ajax.min.js"></script>
    <script src="/scripts/jquery.validate.min.js"></script>
    <script src="/scripts/jquery.validate.unobtrusive.min.js"></script>
    <@stylehead/>
</head>
<body>
<div class="header">
    <span style="color: white; font-size: 25px; margin-left: 25px; font-weight: bold; font-family: 微软雅黑;">Demo</span>
    <div class="fr mr10">
        <span id="toptips" class="remind fl"><span id="toptips1"></span></span>
        <#if user.getCurrent()??>
            <a href="/Account/logout/" class="fl">退出</a>
        <#else>
            <a href="/Account/login/" class="fl">登录</a>
        </#if>
    </div>
</div>
<div style="color:red">${error!}</div>
<div class="content w">
    <@leftmenu/>
    <div class="main_cont">
        <#nested />
    </div>
</div>
</body>
</html>
</#macro>
