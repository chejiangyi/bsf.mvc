﻿<#assign pagetitle="登陆" >

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width"/>
    <title>${pagetitle}!</title>
    <link href="/content/css/css.css" rel="stylesheet"/>
    <link href="/content/css/login.css" rel="stylesheet"/>
    <script type="text/javascript">
    </script>
</head>
<body>
<div class="login">
    <ul class="cont mydetail">
        <form action="#" method="post" name="Form">
            <li><label>springmvcdemo：</label><a href="/springmvcdemo/index/">打开</a>
            </li>
            <li><label>netmvcdemo：</label><a href="/netmvcdemo/index/">打开</a></li>
        </form>
    </ul>
<#if error??>
    <span sytle='color:red'>${error!}</span>
</#if>
</div>
</body>
</html>
