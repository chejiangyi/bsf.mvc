package com.bsf.mvc.webdemo.base;

import com.bsf.mvc.HttpContext;

import java.util.ArrayList;
import java.util.Arrays;

public class User {
    public User getCurrent()
    {
        Object u = HttpContext.getSession().getAttribute("Current");
        if (u == null)
        {
            return null;
        }
        else
        {
            return (User)u;
        }
    }
    public void setCurrent(User value)
    {
        HttpContext.getSession().setAttribute("Current",value);
    }

    public boolean isAdmin()
    {
        return true;
//        if (getCurrent() == null || getCurrent().user_model == null)
//        {
//            return false;
//        }
//        return getCurrent().user_model.role == 0;
    }




}
