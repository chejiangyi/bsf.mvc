package com.bsf.mvc.webdemo.base;

import com.bsf.mvc.template.SimpleTemplateProvider;

/**
 * 底层模板方法的扩展，用于前台freemarker的java函数扩展，方便使用。
 * 但是使用前要么在controller层（SpringMvcController）进行重新注入，要么在类似NetMvcHandlerInterceptor拦截器方式注入重载
 */
public class HtmlHelper extends SimpleTemplateProvider {

    public  String substring3(String str, int maxlen)
    {
       return this.cutstring(str,maxlen);
    }
    public int totalpagenum(int totalRecord,int pageSize)
    {
        return  (totalRecord  +  pageSize  - 1) / pageSize+1;
    }
}
