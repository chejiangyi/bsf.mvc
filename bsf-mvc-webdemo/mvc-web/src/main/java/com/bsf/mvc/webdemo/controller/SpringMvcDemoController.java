package com.bsf.mvc.webdemo.controller;

import com.bsf.mvc.pager.Pager1;
import com.bsf.mvc.webdemo.base.SpringMvcController;
import org.springframework.web.servlet.ModelAndView;

/**
 * sprinboot扩展实现，自动requestmapping（public，返回值为ModelAndView类型/或子类）的方法
 */
public class SpringMvcDemoController extends SpringMvcController {
    public ModelAndView index(Integer a,String b)
    {
        return this.pageVisit(m->{
            //分页控件实现说明
            new Pager1(1,20).setPageSize(10).out();
        });
    }


    public ModelAndView index2()
    {
        return new ModelAndView();
    }
}
