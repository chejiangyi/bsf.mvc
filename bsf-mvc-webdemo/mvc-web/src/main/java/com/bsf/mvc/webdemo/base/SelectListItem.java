package com.bsf.mvc.webdemo.base;

import lombok.Data;
//
@Data
public class SelectListItem {
    public String text="";
    public Object value;
    public boolean selected;
}
