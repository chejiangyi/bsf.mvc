package com.bsf.mvc.webdemo;

import com.bsf.mvc.webdemo.base.NetMvcHandlerInterceptor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;


@SpringBootApplication
public class Application {
    @Configuration
    public class CustomWebConfig extends com.bsf.mvc.web.WebConfig {
        @Override
        public void addInterceptors(InterceptorRegistry registry) {
            registry.addInterceptor(new NetMvcHandlerInterceptor());
            super.addInterceptors(registry);
        }

        //首页配置
        @Override
        public void addViewControllers(ViewControllerRegistry registry) {

            registry.addViewController("/").setViewName("forward:/home/");
            registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
            super.addViewControllers(registry);
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
