package com.bsf.mvc.webdemo.controller;

import com.bsf.mvc.pager.Pager1;
import com.bsf.mvc.webdemo.base.HtmlHelper;
import com.bsf.mvc.webdemo.base.NetMvcController;
import com.bsf.mvc.webdemo.base.User;

import java.util.ArrayList;
import java.util.List;

/**
 * 扩展springboot 模拟.net mvc的写法
 * 需要继承 NetController
 */
public class NetMvcDemoController extends NetMvcController {
    public ActionResult index(Integer a,String b)
    {
        //分页控件实现说明
        new Pager1(1,20).setPageSize(10).out();
        return view();
    }

    public ActionResult index2()
    {
        List a = new ArrayList<String>();
        return view("/netmvcdemo/index2",a);
    }

    public ActionResult json()
    {
        List a = new ArrayList<String>();
        return json(a);
    }
}
