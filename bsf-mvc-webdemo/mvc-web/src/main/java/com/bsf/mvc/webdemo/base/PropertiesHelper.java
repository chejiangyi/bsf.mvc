package com.bsf.mvc.webdemo.base;

import com.bsf.util.PropertiesUtil;

public class PropertiesHelper {
    private static final String filename = "application.properties";
    public static String read(String key)
    {
       return   PropertiesUtil.read(PropertiesUtil.tryfindPropertiesPath(filename,PropertiesHelper.class),key);
    }


}
