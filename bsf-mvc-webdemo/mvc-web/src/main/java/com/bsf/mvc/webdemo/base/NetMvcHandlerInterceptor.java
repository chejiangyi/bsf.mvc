package com.bsf.mvc.webdemo.base;

import com.bsf.mvc.controller.NetController;
import com.bsf.mvc.template.SimpleTemplateProvider;
import com.bsf.mvc.template.TemplateProvider;
import com.bsf.serialization.json.JsonProvider;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Net Mvc 使用拦截器的用法方式，实现类似aop的demo
 */
public class NetMvcHandlerInterceptor implements HandlerInterceptor {

    /**
     * Handler执行完成之后调用这个方法
     */
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception exc)
            throws Exception {

    }

    /**
     * Handler执行之后，ModelAndView返回之前调用这个方法
     */
    public void postHandle(HttpServletRequest request, HttpServletResponse response,
                           Object handler, ModelAndView modelAndView) throws Exception {
    }

    /**
     * Handler执行之前调用这个方法
     */
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler) throws Exception {
        if(handler instanceof HandlerMethod) {
            HandlerMethod h = (HandlerMethod)handler;
            if (h.getBean() instanceof NetController) {
                //重载Html扩展方法
                request.setAttribute("Html",new HtmlHelper());
                request.setAttribute("user",new User());

            }
        }
        return true;
    }

}