package com.bsf.mvc.webdemo.controller;


import com.bsf.mvc.webdemo.base.SpringMvcController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


/**
 * spingboot的正常用法，还可以继续使用。
 * 自动requestmapping只是约定默认实现。
 */
@Controller
@RequestMapping("/systemerror")
public class SystemErrorController extends SpringMvcController  {
@RequestMapping("/index")
    public ModelAndView index() {
        return this.pageVisit((m)->{
        });
    }

}
